from conans import ConanFile, tools

class Sol2(ConanFile):
    name = 'sol2'
    version = '2.20.4'
    url = 'https://gitlab.com/monlib/sol2-conan.git'
    no_copy_source = True

    def source(self):
        downloadRoot = 'https://github.com/ThePhD/sol2/releases/download/v' + self.version
        tools.download(downloadRoot + '/sol.hpp', 'sol.hpp')
        tools.download(downloadRoot + '/sol_forward.hpp', 'sol_forward.hpp')

    def package(self):
        self.copy('*.hpp', dst='include', src='.')
